#Locates vtables and creates types for them
#
#This should work with any Itanium ABI binary
#Inspired by https://alschwalm.com/blog/static/2017/01/24/reversing-c-virtual-functions-part-2-2/
#@author David Konarik <dvd.kon@gmail.com>
#@category User
#@keybinding 
#@menupath 
#@toolbar 

from __future__ import print_function

import re
import timeit
import os
import json 
import atexit
from collections import namedtuple

from ghidra.program.util import ProgramMemoryUtil
from ghidra.program.model.data import (
    StructureDataType, BuiltInDataTypeManager, CategoryPath,
    FunctionDefinitionDataType, StringDataInstance)
from ghidra.program.model.symbol import SourceType
from ghidra.program.model.address import Address
from ghidra.app.util.demangler.gnu import GnuDemanglerNativeProcess

# If set to a string path, it will put debug logs and persist some data there
SAVE_LOCATION = None

MAIN_TYPE_CATEGORY_ID = 0 # My guess is it's always 0
SIZE_T = 4

dtm = currentProgram.getDataTypeManager()
bdtm = BuiltInDataTypeManager.getDataTypeManager()

if SIZE_T == 8:
    getSizet = getLong
    sizet_dt = bdtm.getDataType("/long")
elif SIZE_T == 4:
    getSizet = getInt
    sizet_dt = bdtm.getDataType("/int")
else:
    raise Exception("Unsupported word size")

VTable = namedtuple("VTable", [
    "addr", "length", "offset_to_top", "typeinfo", "funcs"])
class Kind:
    CLASS = "cls"
    POINTER = "ptr"
TypeInfo = namedtuple("TypeInfo", [
    "addr", "length", "kind", "typename", "bases", "vmi_flags"])
BaseClass = namedtuple("BaseClass", [
    "typeinfo", "offset", "is_virtual", "is_public"])

def normalize_addr_to_int(a):
    if isinstance(a, Address): return a.getOffset()
    else: return a

class AddrDict(dict):
    def __init__(self, d={}):
        dict.__init__(self)
        for k, v in d.items():
            self[k] = v
    def __getitem__(self, i):
        return dict.__getitem__(self, normalize_addr_to_int(i))
    def __setitem__(self, i, v):
        return dict.__setitem__(self, normalize_addr_to_int(i), v)
    def __delitem__(self, i):
        return dict.__delitem__(self, normalize_addr_to_int(i))
    def __contains__(self, i):
        return dict.__contains__(self, normalize_addr_to_int(i))
    def get(self, key, default=None):
        if key in self: return self[key]
        else: return default

def read_data_refs(skip_linker_blocks=True):
    # Looks at all aligned SIZE_T data in all "data sections" (no execute
    # permission) and puts them into a dict of references.
    refs = AddrDict()

    mem = currentProgram.getMemory()
    mem_addrset = mem.getAllInitializedAddressSet()

    for block in mem.getBlocks():
        if skip_linker_blocks:
            if block.getName() in ["EXTERNAL", "__LINKEDIT"]: continue
        if block.getPermissions() & block.EXECUTE != 0: continue
        if not block.isInitialized(): continue
        addr = block.getStart()
        if addr.getAddressSpace().getName() != "ram": continue
        addr_int = addr.getOffset()
        end = block.getStart().add((block.getSize() // SIZE_T) * SIZE_T)
        while addr < end:
            val = getSizet(addr)
            if mem_addrset.contains(toAddr(val)):
                if val not in refs: refs[val] = []
                refs[val].append(addr_int)
            addr = addr.add(SIZE_T); addr_int += SIZE_T

    return refs

def symbol_namespace_str(sym):
    path_elems = []
    ns = sym
    while not ns.isGlobal():
        path_elems.append(ns.getName())
        ns = ns.getParentNamespace()
    path_elems.reverse()
    return "::".join(path_elems)

def is_valid_addr(addr):
    mem = currentProgram.getMemory()
    mem_start = mem.getFirstRange().getMinAddress().getOffset()
    mem_end = mem.getLastRange().getMaxAddress().getOffset()
    return mem_start <= addr and addr <= mem_end

def is_addr_in_exec_block(addr):
    mb = getMemoryBlock(addr)
    if mb is None: return False
    return mb.getPermissions() & mb.EXECUTE > 0

def is_addr_in_data_block(addr):
    mb = getMemoryBlock(addr)
    if mb is None: return False
    return mb.getPermissions() & mb.READ > 0 \
        and mb.getPermissions() & mb.EXECUTE == 0

def is_addr_readable(addr):
    """A hack for get... funcs not being able to access EXTERNAL"""
    try:
        getByte(addr)
        return True
    except:
        return False

def find_vtables_by_symbol():
    syms = currentProgram.getSymbolTable().getDefinedSymbols()
    return ((symbol_namespace_str(s.getParentNamespace()), s.getAddress())
            for s in syms 
            if s.getName() == "vtable")

def read_vtable(vt_addr):
    # Unfortunately, memory in EXTERNAL can't be read by getLong etc., so this
    # function will return some "invalid" addresses.
    # You can filter the results with is_addr_readable()
    ott = getSizet(vt_addr)
    ti_addr = toAddr(getSizet(vt_addr.add(SIZE_T)))
    addr = vt_addr.add(SIZE_T * 2) # Skip offset to top and RTTI ptr
    funcs = []
    # Sometimes destructors (but not other functions) are zeroed out if they're
    # not explicitly defined. VTables shouldn't ever have zero functions
    # anyway, so this shouldn't hurt.
    while True:
        val = getSizet(addr)
        if val == 0:
            funcs.append(None)
        else: break
        addr = addr.add(SIZE_T)
    while True:
        val = toAddr(getSizet(addr))
        sym = getSymbolAt(val)
        if sym is not None and re.match("_*__cxa_pure_virtual$", sym.getName()):
            funcs.append(None)
        elif not is_addr_in_exec_block(val):
            break
        else:
            funcs.append(val)
        addr = addr.add(SIZE_T)
    return VTable(vt_addr, addr.subtract(vt_addr), ott, ti_addr, funcs)

def find_vtables_in_block(start_addr):
    addr = start_addr
    min_ott = 1
    while True:
        ott = getSizet(addr)
        if ott < -0x8000 or ott > 0 or ott >= min_ott: break
        min_ott = ott
        vt = read_vtable(addr)
        yield vt
        addr = addr.add(SIZE_T * (2 + len(vt.funcs)))

def get_vtable_func_objs(vt):
    for func in vt.funcs:
        if func is None: yield None
        else:
            yield getFunctionAt(func)

def guess_funcs_from_children(ti_addr, all_tis, ti_children, vts_for_ti):
    """
    vts_for_ti has to be sorted by occurrence in the binary!
    """
    imm_children = []
    bases = [ti_addr]
    start_vts = vts_for_ti[ti_addr]
    if len(start_vts) > 1:
        log("ERR: Can't guess funcs for class with multiple VTs:", ti_addr)
        return None
    elif len(start_vts) == 1:
        funcs = list(get_vtable_func_objs(start_vts[0]))
        func_count = len(funcs)
        found_all_names = \
            not any(n is None for n in funcs) \
            and len(funcs) > 0
    else:
        funcs = []
        func_count = 2 << 16 # Arbitrary large number
        found_all_names = False
    while len(bases) > 0 and not found_all_names:
        for base in bases:
            for child in ti_children[base]:
                child_ti = all_tis[child]
                for i, child_base in enumerate(child_ti.bases):
                    if child_base.typeinfo == base:
                        child_base_i = i
                vts = vts_for_ti[child]
                if len(vts) <= child_base_i: continue # TODO: Optional logging?
                vt = vts[child_base_i]
                func_count = min(func_count, len(vt.funcs))
                funcs = [funcs[i] if i < len(funcs) else None
                             for i in range(func_count)]
                funcs_new = list(get_vtable_func_objs(vt))[:func_count]
                for i, func in enumerate(funcs_new):
                    if func is None: continue
                    funcs[i] = func
                found_all_names = \
                    not any(n is None for n in funcs)
                if found_all_names: break
                bases.append(child)
            if found_all_names: break
            bases.remove(base)
        if found_all_names: break
    return funcs

def demangle_typename(mangled):
    if not re.match("^N?[1-9][0-9]*[A-Za-z_][A-Za-z0-9_]*$", mangled):
        return None
    # GnuDemangler is really buggy, it seems like some typenames "linger"
    np = GnuDemanglerNativeProcess.getDemanglerNativeProcess()
    dm = np.demangle("_ZTS" + mangled).strip()
    # This should cover all eventualities
    if dm == "_ZTS" + mangled or dm == mangled or len(dm) == 0 or dm == "_Z":
        return None
    log_to_file("demangle_log", "%s -> %s\n" % (mangled, dm))
    prefix = "typeinfo name for "
    if not dm.startswith(prefix): return None
    dm = dm[len(prefix):]
    dm = dm.replace(" ", "") # Don't confuse Ghidra
    dm = re.sub("[^<]<(.*)>", lambda tpl: tpl.group(0).replace(":", "-"), dm)
    if dm[-1] == "*":
        return (Kind.POINTER, dm)
    else:
        return (Kind.CLASS, dm)
    # TODO: Other kinds?

def find_typeinfos_by_typename(datarefs):
    all_data = currentProgram.getListing().getDefinedData(toAddr(0), True)
    for data in all_data:
        if not StringDataInstance.isString(data): continue
        mangled = data.getValue()
        demangled = demangle_typename(mangled)
        if demangled is None: continue
        refs = datarefs.get(data.getAddress(), None)
        if refs is None:
            log("ERR: No references to typename", data.getAddress())
            continue
        if len(refs) > 1:
            log("ERR: Multiple references to typename", data.getAddress())
            continue
        yield toAddr(next(iter(refs))).add(-SIZE_T)

def read_typeinfo(ti_addr):
    # TODO: This could use symbols for std::type_info vtables for some added
    # precision
    typename_mangled = \
        getDataAt(toAddr(getSizet(ti_addr.add(SIZE_T)))).getValue()
    kind, typename = demangle_typename(typename_mangled)

    if kind == Kind.CLASS:
        bases_addr = ti_addr.add(SIZE_T * 2) # Skip vptr and __type_name
        as_addr = toAddr(getSizet(bases_addr))
        as_flags = getInt(bases_addr)
        as_basecount = getInt(bases_addr.add(4))
        vmi_flags = None
        length = SIZE_T * 2
        # is_addr_readable could cause some legitimate bases to be dropped here
        # TODO: What goes in EXTERNAL, and why is it unreadable, anyway?
        if is_addr_in_data_block(as_addr) and is_addr_readable(as_addr):
            # Probably __si_class_type_info
            bases = [BaseClass(as_addr, 0, False, True)]
            length += SIZE_T
        # 10 is just some arbitrary chosen value, a class is unlikely to have
        # more than 10 base classes
        # TODO: Are flags also 4 bytes on 32-bit ABI?
        elif as_basecount in range(1, 10) \
            and as_flags in range(4): # Acceptable values: 1 and 2
            # Probably __vmi_class_type_info
            vmi_flags = getInt(bases_addr)
            bases = []
            length += 8
            for i in range(getInt(bases_addr.add(4))):
                addr = bases_addr.add(SIZE_T * (i*2 + 1))
                bti = toAddr(getSizet(addr))
                composite = getSizet(addr.add(SIZE_T))
                offset = composite >> 8
                flags = composite & 0xFF
                bases.append(BaseClass(
                    bti, offset, bool(flags & 1), bool(flags & 2)))
                length += SIZE_T*2
        else: # __class_type_info, I guess. *shrug*
            bases = []
    else:
        bases = None
        vmi_flags = None
    return TypeInfo(ti_addr, length, kind, typename, bases, vmi_flags)

def get_vtables_for_typeinfo(ti_addr, datarefs, all_vts=None, all_tis=None):
    """
    Will try to find a vtable belonging to a typeinfo by looking for direct
    references. If provided with a dict of all VTs in the program, it will
    match the references against that dict. A dict of all TIs can be provided
    as a blacklist.
    """
    refs = datarefs.get(ti_addr, [])
    vts = []
    for r in refs:
        r = toAddr(r)
        vt_addr = r.add(-SIZE_T)
        ott = getSizet(vt_addr)
        # Presumes no object will be larger than 8kB
        if ott < -8*1024 or ott > 0: continue
        if all_vts is not None:
            if vt_addr in all_vts:
                vts.append(vt_addr)
        elif all_tis is not None:
            if not any(r > ti.addr and r < ti.addr.add(ti.length)
                       for ti in all_tis.values()):
                vts.append(vt_addr)
        else:
            vts.append(vt_addr)
    return vts

def get_typeinfo_children(all_tis):
    children = AddrDict({ti.addr: [] for ti in all_tis.values()
                         if ti.kind == Kind.CLASS})
    for ti in all_tis.values():
        if ti.kind != Kind.CLASS: continue
        for base in ti.bases:
            if base.typeinfo not in children: continue # External class, TODO
            children[base.typeinfo].append(ti.addr)
    return children

def namespace_str_to_type_category(nsstr):
    return "/" + "/".join(nsstr.split("::"))

def create_struct_type(path):
    dt = dtm.getDataType(path)
    if dt is None:
        cp = CategoryPath("/".join(path.split("/")[:-1]))
        cat = dtm.createCategory(cp)
        dt = StructureDataType(cp, path.split("/")[-1], 0, dtm)
        return dtm.addDataType(dt, None)
    else:
        return dt

def create_vtable_header_type():
    dt = create_struct_type("/vtable_header")
    if dt.getNumComponents() > 0: return
    dt.add(sizet_dt, SIZE_T, "offset_to_top", "")
    # TODO: Struct for at least the two common fields?
    dt.add(bdtm.getDataType("/pointer"), SIZE_T, "typeinfo", "")
    dtm.addDataType(dt, None)

def create_typeinfo_class_type():
    dt = create_struct_type("/__class_type_info")
    if dt.getNumComponents() > 0: return
    dt.add(bdtm.getDataType("/pointer"), SIZE_T, "vptr", "")
    charptr = bdtm.getPointer(bdtm.getDataType("/char"))
    dt.add(charptr, SIZE_T, "__type_name", "")

def create_typeinfo_si_class_type():
    dt = create_struct_type("/__si_class_type_info")
    if dt.getNumComponents() > 0: return
    dt.add(bdtm.getDataType("/pointer"), SIZE_T, "vptr", "")
    charptr = bdtm.getPointer(bdtm.getDataType("/char"))
    dt.add(charptr, SIZE_T, "__type_name", "")
    dt.add(bdtm.getDataType("/pointer"), SIZE_T, "__base_type", "")

def create_typeinfo_base_class_type():
    dt = create_struct_type("/__base_class_type_info")
    if dt.getNumComponents() > 0: return
    dt.add(bdtm.getDataType("/pointer"), SIZE_T, "__base_type", "")
    dt.add(bdtm.getDataType("/long"), SIZE_T, "__offset_flags", "")

def create_typeinfo_vmi_class_type():
    dt = create_struct_type("/__vmi_class_type_info")
    if dt.getNumComponents() > 0: return
    dt.add(bdtm.getDataType("/pointer"), SIZE_T, "vptr", "")
    charptr = bdtm.getPointer(bdtm.getDataType("/char"))
    dt.add(charptr, SIZE_T, "__type_name", "")
    dt.add(bdtm.getDataType("/int"), 4, "__flags", "")
    dt.add(bdtm.getDataType("/int"), 4, "__base_count", "")
    dt.setFlexibleArrayComponent(
        dtm.getDataType("/__base_class_type_info"), "__base_info", "")

def get_func_str_repr(fsig):
    out = fsig.getPrototypeString()
    out = out.replace("*", "ptr")
    out = re.sub("[^a-zA-Z0-9]", "_", out)
    return out

def get_type_for_func(fsig):
    frepr = get_func_str_repr(fsig)
    t = dtm.getDataType("/funcptrs/" + frepr)
    if t is not None: return t
    catp = CategoryPath("/funcptrs")
    dtm.createCategory(catp)
    functype = FunctionDefinitionDataType(fsig)
    functype.setNameAndCategory(
        catp, get_func_str_repr(functype))
    return functype

def create_vtable_datatypes(ti, vts_for_ti, all_tis, ti_children):
    path = namespace_str_to_type_category(ti.typename) + "::vtable"
    ptr_type = bdtm.getPointer(bdtm.getDataType("/void"))
    for i, vt in enumerate(vts_for_ti[ti.addr]):
        i = "" if i == 0 else i + 1
        path_numd = path + str(i)
        dt = create_struct_type(path_numd)
        dt.deleteAll()
        funcs = list(get_vtable_func_objs(vt))
        if len(funcs) == 0 or any(fn is None for fn in funcs):
            func_guess = guess_funcs_from_children(
                ti.addr, all_tis, ti_children, vts_for_ti)
            funcs = func_guess # TODO: Allow classes with partial info
        if funcs is not None:
            for f in funcs:
                if f is None:
                    name = None
                    ptr = ptr_type
                else:
                    name = f.getName()
                    f.setCallingConvention("__thiscall")
                    fsig = f.getSignature()
                    fsig.replaceArgument(
                        0, "this", ptr_type, None, SourceType.ANALYSIS)
                    functype = get_type_for_func(fsig)
                    ptr = dtm.getPointer(functype)
                dt.add(ptr, SIZE_T, name, "")
        yield dt

def add_type_field_at(dt, cdt, offset, name, desc):
    clen = cdt.getLength()
    dtlen = 0 if dt.getNumComponents() == 0 else dt.getLength()
    dt.growStructure(max(0, offset - dtlen))
    j = offset
    to_delete = []
    if dt.getNumComponents() > 0:
        while j < offset + clen:
            if j >= dtlen: break
            c = dt.getComponentAt(j)
            j = c.getOffset() + c.getLength()
            to_delete.append(c.getOrdinal())
    dt.delete(to_delete)
    dt.insertAtOffset(offset, cdt, clen, name, desc)

def flatten_bases(ti, all_tis):
    fbases = []
    for base in ti.bases:
        fbases.append((base.offset, ))

def add_vptrs_to_datatype(ti, vt_dts, all_tis):
    path = namespace_str_to_type_category(ti.typename)
    dt = create_struct_type(path)
    if len(ti.bases) > len(vt_dts):
        log("ERR: Not enough VTs for all bases of TI", ti.addr)
        return
    if len(ti.bases) == 0:
        if len(vt_dts) > 1:
            log("ERR: Too many VTs for base TI", ti.addr)
            return
        if len(vt_dts) < 1:
            log("ERR: No VT for base TI", ti.addr)
            return
        vtptr = dtm.getPointer(vt_dts[0])
        add_type_field_at(dt, vtptr, 0, "vptr", "vtable")
    for i, base in enumerate(ti.bases):
        if base.offset < 0:
            # TODO: Virtual bases
            log("ERR: Virtual base %d of TI %s" % (i, ti.addr))
            continue
        vtptr = dtm.getPointer(vt_dts[i])
        field_name = "vptr" + ("" if i == 0 else str(i + 1))
        if base.typeinfo not in all_tis: # External class
            desc = "external base vtable"
        else:
            desc = all_tis[base.typeinfo].typename + " base vtable"
        add_type_field_at(dt, vtptr, base.offset, field_name, desc)

def create_data_at_vtables(vts, dts):
    vth_dt = getDataTypes("vtable_header")[0]
    for i, vt in enumerate(vts):
        clearListing(vt.addr, vt.addr.add(vt.length))
        createData(vt.addr, vth_dt)
        createData(vt.addr.add(SIZE_T * 2), dts[i])

def create_data_at_typeinfo(ti):
    clearListing(ti.addr, ti.addr.add(ti.length))
    if ti.vmi_flags is not None:
        createData(ti.addr, dtm.getDataType("/__vmi_class_type_info"))
        for i in range(len(ti.bases)):
            createData(ti.addr.add(SIZE_T*2 + 8 + (SIZE_T*2)*i),
                       dtm.getDataType("/__base_class_type_info"))
    elif len(ti.bases) == 0:
        createData(ti.addr, dtm.getDataType("/__class_type_info"))
    else:
        createData(ti.addr, dtm.getDataType("/__si_class_type_info"))

def timer_start():
    global timer_section_start
    timer_section_start = timeit.default_timer()

def timer_section(name):
    global timer_section_start
    time = timeit.default_timer() - timer_section_start
    timer_section_start = timeit.default_timer()
    log("-- %s took %.2fs" % (name, time))

json_named_tuples = {
    "VTable": VTable,
    "TypeInfo": TypeInfo,
    "BaseClass": BaseClass,
}

def json_object_hook(obj):
    if "_type" in obj:
        t = obj["_type"]
        if t == "Address":
            return toAddr(obj["addr"])
        if t == "AddrDict":
            return AddrDict(obj["data"])
        if t in json_named_tuples:
            return json_named_tuples[t](**obj["data"])
        raise Exception("Unknown JSON _type: " + t)
    else:
        obj = dict((int(k) if k.isnumeric() else k, v)
                   for k, v in obj.items())
        return obj

def json_load(name):
    if SAVE_LOCATION is None: return None
    path = SAVE_LOCATION + os.path.sep + name
    if not os.path.exists(path): return None
    with open(path, "rb") as f:
        return json.load(f, object_hook=json_object_hook)

class JsonEncoder(json.JSONEncoder):
    def modify(self, obj):
        if hasattr(obj, "_asdict"): # Probably a NamedTuple
            return {
                "_type": type(obj).__name__,
                "data": {k: self.modify(v)
                         for k, v in obj._asdict().items()}
            }
        elif isinstance(obj, AddrDict):
            return {
                "_type": "AddrDict",
                "data": {k: self.modify(v)
                         for k, v in obj.items()}
            }
        elif isinstance(obj, Address):
            return {"_type": "Address", "addr": obj.getOffset()}
        elif isinstance(obj, list):
            return [self.modify(i) for i in obj]
        elif isinstance(obj, dict):
            return {k: self.modify(v) for k, v in obj.items()}
        else:
            return obj

    def encode(self, obj):
        to_enc = self.modify(obj)
        return json.JSONEncoder.encode(self, to_enc)

def json_save(name, data):
    if SAVE_LOCATION is None: return
    path = SAVE_LOCATION + os.path.sep + name
    with open(path, "w") as f:
        # Dump doesn't call .encode(). Why? Dunno...
        f.write(json.dumps(data, cls=JsonEncoder))

def log_to_file(name, line):
    if SAVE_LOCATION is None: return
    path = SAVE_LOCATION + os.path.sep + name
    with open(path, "a") as f:
        f.write(line + "\n")

if SAVE_LOCATION is None:
    logfile = None
else:
    path = SAVE_LOCATION + os.path.sep + "log"
    logfile = open(path, "w")
    # XXX: Will this run when the Python script exits or when Ghidra stops?
    atexit.register(lambda f: f.close(), logfile)
def log(*args, **kwargs):
    print(*args, file=logfile, **kwargs)
    print(*args, **kwargs)

timer_start()

create_vtable_header_type()
create_typeinfo_class_type()
create_typeinfo_si_class_type()
create_typeinfo_base_class_type()
create_typeinfo_vmi_class_type()
timer_section("Creating Itanium ABI types")

datarefs = json_load("datarefs")
if datarefs is None:
    datarefs = read_data_refs()
    json_save("datarefs", datarefs)
timer_section("Finding data references")

all_ti_addrs = json_load("all_ti_addrs")
if all_ti_addrs is None:
    all_ti_addrs = list(find_typeinfos_by_typename(datarefs))
    json_save("all_ti_addrs", all_ti_addrs)
timer_section("Finding all TIs")

all_tis = AddrDict({a: read_typeinfo(a) for a in all_ti_addrs})
json_save("all_tis", all_tis)
timer_section("Reading all TIs")

vts_for_ti = json_load("vts_for_ti")
all_vts = json_load("all_vts")
if vts_for_ti is None or all_vts is None:
    vts_for_ti = AddrDict({a: [] for a in all_tis.keys()})
    all_vts = AddrDict()
    for ti in all_tis.values():
        if ti.kind != Kind.CLASS: continue
        vtas = get_vtables_for_typeinfo(ti.addr, datarefs, all_tis=all_tis)
        for vta in vtas:
            vt = read_vtable(vta)
            all_vts[vta] = vt
            vts_for_ti[ti.addr].append(vt)
        vts_for_ti[ti.addr].sort(key=lambda vt: -vt.offset_to_top)

    json_save("vts_for_ti", vts_for_ti)
    json_save("all_vts", all_vts)
timer_section("Finding all VTs")

ti_children = json_load("ti_children")
if ti_children is None:
    ti_children = get_typeinfo_children(all_tis)
    json_save("ti_children", ti_children)
timer_section("Finding TI children")

for ti in all_tis.values():
    try:
        if ti.kind != Kind.CLASS: continue
        create_data_at_typeinfo(ti)
        dts = list(create_vtable_datatypes(ti, vts_for_ti, all_tis, ti_children))
        add_vptrs_to_datatype(ti, dts, all_tis)
        create_data_at_vtables(vts_for_ti[ti.addr], dts)
    except:
        log("Error while creating datatypes for", ti.addr)
        raise
timer_section("Creating datatypes")
