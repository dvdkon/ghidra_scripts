#This script will iterate over all identified methods and set their calling
#convention to __thiscall
#This doesn't deal with static methods, but thiscall is a much better
#default.
#@author David Konarik <dvd.kon@gmail.com>
#@category User
#@keybinding 
#@menupath 
#@toolbar 

# This approach fails to deal with classes in namespaces.
# Ghidra API bug or just weirdness?

#from ghidra.program.database.symbol import FunctionSymbol
#
#st = getCurrentProgram().getSymbolTable()
#classes = st.getClassNamespaces()
#
#for cls in classes:
#    syms = st.getSymbols(cls)
#    for sym in syms:
#        if not isinstance(sym, FunctionSymbol): continue
#        fun = getFunctionAt(sym.getAddress())
#        fun.setCallingConvention("__thiscall")

from ghidra.program.database.symbol import GhidraClassDB

funcs = getCurrentProgram().getFunctionManager().getFunctions(toAddr(0), True)
for func in funcs:
    pn = func.getParentNamespace()
    if not isinstance(pn, GhidraClassDB): continue
    func.setCallingConvention("__thiscall")
