# Get the names of all functions defined by the user
#@author David Konarik <dvd.kon@gmail.com>
#@category User
#@keybinding 
#@menupath 
#@toolbar 

USER_DEFINED = ghidra.program.model.symbol.SourceType.USER_DEFINED

funcs = getCurrentProgram().getListing().getFunctions(toAddr(0), True)
for func in funcs:
	sig_src = func.getSignatureSource()
	if sig_src == USER_DEFINED:
		print("%s\t%s" % (func.getEntryPoint(), func.getName()))
