#Finds types with duplicate names and deletes redundant versions
#
#For some reason, autoanalysis creates types both in the root category and in
#"/Demangler". My VTable finding script creates all types in / (because why
#not), so duplicates are created. This script deletes the "/Demangler" version.
#@author David Konarik <dvd.kon@gmail.com>
#@category User

from ghidra.program.model.data import CategoryPath

DELETE_FROM = ["/Demangler"]
DELETE_FROM_CATS = [CategoryPath(c) for c in DELETE_FROM]

dtm = getCurrentProgram().getDataTypeManager()

dts_by_name = {}

def process_tcat(cat):
    for ccat in cat.getCategories():
        process_tcat(ccat)
    for dt in cat.getDataTypes():
        name = dt.getName()
        if name not in dts_by_name:
            dts_by_name[name] = set()
        dts_by_name[name].add(dt)

process_tcat(dtm.getCategory(0)) # 0 - the program's root category

for name, dts in dts_by_name.items():
    if len(dts) > 1:
        for dt in dts:
            if any(dt.getCategoryPath().isAncestorOrSelf(c)
                   for c in DELETE_FROM_CATS):
                print("Deleting " + str(dt.getDataTypePath()))
                dtm.remove(dt, monitor)
